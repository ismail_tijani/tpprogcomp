import {createStore, combineReducers, applyMiddleware} from 'redux';
import {persistStore, persistReducer} from 'redux-persist'; // imports from redux-persist
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web
import thunkMiddleware from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension/developmentOnly'; // this is for debugging with React-Native-Debugger, you may leave it out


import {loginReducer} from './auth/reducer';
import {articlesReducer} from './article/reducer';
import {categoriesReducer} from './categorie/reducer';

const rootReducer = combineReducers({
    loginReducer: loginReducer,
    articlesReducer: articlesReducer,
    categoriesReducer: categoriesReducer,
});
const persistConfig = {
    key: "root",
    storage: storage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
export const store = createStore(persistedReducer,
    composeWithDevTools(applyMiddleware(thunkMiddleware)),
);

export const persistor = persistStore(store);
