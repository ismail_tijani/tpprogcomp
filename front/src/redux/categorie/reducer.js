import {initialState} from './initialState';
import * as t from '../actionTypes';

export const categoriesReducer = (state = initialState, action) => {
    switch (action.type) {
        case t.SET_CATEGORIES_STATE:
            return {
                ...state,
                categories: action.payload, // this is what we expect to get back from API call and login page input
            };
        default:
            return state;
    }
};
