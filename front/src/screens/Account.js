import React, {useCallback, useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import {useParams} from "react-router-dom";

import {logout, setLoginState} from "../redux/auth/actions";
import {connect} from "react-redux";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        margin: 'auto',
        maxWidth: 500,
    },
    image: {
        width: 128,
        height: 128,
    },
    img: {
        margin: 'auto',
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%',
    },
}));


const Account = (props) => {
    const classes = useStyles();
    const {id} = useParams();
    const {catId} = useParams();


    useEffect(() => {
        console.log(props.auth.articles)
    }, []);


    return (
        <React.Fragment>
            <CssBaseline />
            <main>
                <div className={classes.heroContent}>
                    <Container maxWidth="sm">
                        <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                            Vos Achats
                        </Typography>
                    </Container>
                </div>

                {props.auth.articles.map((art)=>(
                    <div className={classes.root}>
                        <Paper className={classes.paper}>
                            <Grid container spacing={3}>
                                <Grid item>
                                    <ButtonBase className={classes.image}>
                                        <img className={classes.img} alt="complex" src="https://source.unsplash.com/random" />
                                    </ButtonBase>
                                </Grid>

                                <Grid item xs={12} sm container>
                                    <Grid item xs container direction="column" spacing={2}>
                                        <Grid item xs>
                                            <Typography gutterBottom variant="subtitle1">
                                                {art.name}
                                            </Typography>
                                            <Typography gutterBottom variant="subtitle1">
                                                {art.type ==='vendu' ? 'Acheté En achat Direct' :''}
                                                {art.type ==='enchere' ? `Encheri Pour: ${art.currentPrice} €`  :''}
                                            </Typography>
                                        </Grid>

                                    </Grid>
                                    <Grid item>
                                        <Typography variant="subtitle1">
                                            € {art.price}
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Paper>
                        <br></br>
                    </div>
                    ))
                }
            </main>
        </React.Fragment>
    );


}

const mapStateToProps = (store) => {
    return {
        auth: store.loginReducer,
    };
};

const mapDispatchToProps = {
    logout,
    setLoginState
};

export default connect(mapStateToProps, mapDispatchToProps)(Account);


// export  default  ArticleView;
