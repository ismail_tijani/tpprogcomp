import React, {useEffect} from "react";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { store, persistor } from "./redux/store";
import Header from "./components/Header";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { useHistory } from "react-router-dom";

import {
  BrowserRouter as Router,
} from "react-router-dom";
import Routes from "./Routes";

const  App = () => {
    const history = useHistory();


    useEffect(() => {
  });

  return (
      <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
              <Router>
                <Header/>
                <Routes/>
            </Router>
              <ToastContainer/>

          </PersistGate>
      </Provider>
  );
}

export default App;
