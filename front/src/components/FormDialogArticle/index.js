import React, {useRef, useEffect} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import MenuItem from '@material-ui/core/MenuItem';

import {connect} from "react-redux";
import {getCat} from "../../redux/categorie/actions";

const FormDialogArticle = (props) => {
    const [open, setOpen] = React.useState(false);
    const [name, setName] = React.useState('');
    const [description, setDescription] = React.useState('');
    const [color, setColor] = React.useState('');
    const [price, setPrice] = React.useState(0);
    const [categorieId, setCategorieId] = React.useState(null);

    const handleChange = (value) => {
        if(value.target.id =='name'){
            setName(value.target.value)
        }
        if(value.target.id =='color'){
            setColor(value.target.value)
        }
        if(value.target.id =='description'){
            setDescription(value.target.value)
        }
        if(value.target.id =='price'){
            setPrice(value.target.value)
        }

    }

    const handleSelect = (value) => {
        setCategorieId(value.target.value)
    }

    const handleSubmit = () => {
        let data = {
            name: name,
            description: description,
            color: color,
            price: price,
            id: categorieId
        }

        props.onSubmit(data);

        handleClose();

    }
    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        setCategorieId(null);
        setName('');
        setColor('');
        setDescription('');
    };

    useEffect(() => {
        props.handleRef.current = handleClickOpen;
        props.getCat()

        return () => {
            props.handleRef.current = null;
        };
    }, []);

    return (
        <div>

            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Création Article</DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Nom"
                        type="email"
                        required={true}
                        onChange={(el)=> handleChange(el)}
                        fullWidth
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        id="description"
                        label="Description"
                        type="text"
                        onChange={(el)=> handleChange(el)}
                        fullWidth
                    />
                    <TextField
                        autoFocus
                        margin="dense"
                        id="color"
                        label="Couleur"
                        type="text"
                        onChange={(el)=> handleChange(el)}
                        fullWidth
                    />

                    <TextField
                        id="categorieId"
                        margin="dense"
                        select
                        label="Select"
                        // value={categorieId}
                        onChange={(el)=> handleSelect(el)}
                        helperText="selectionnez categorie"
                        fullWidth
                    >
                        {props.data.categories.map((option) => (
                            <MenuItem key={option.id} value={option.id}>
                                {option.name}
                            </MenuItem>
                        ))}
                    </TextField>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="price"
                        label="Prix"
                        type="number"
                        onChange={(el)=> handleChange(el)}
                        fullWidth

                    />
                </DialogContent>
                <DialogActions>
                    <Button variant="contained" onClick={handleClose} color="primary">
                        Annuler
                    </Button>
                    <Button variant="contained" onClick={handleSubmit} color="primary">
                        Valider
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}


const mapStateToProps = (store) => {
    return {
        data: store.categoriesReducer,
    };
};
const mapDispatchToProps = {
    getCat
};
export default connect(mapStateToProps, mapDispatchToProps)(FormDialogArticle);


