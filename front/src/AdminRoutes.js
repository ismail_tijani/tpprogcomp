import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";

import Articles from "./screens/Admin/Articles"
import Categories from "./screens/Admin/Categories"
import Dashboard from "./screens/Admin/Dashboard"

const AdminRoutes = () => {
    return (
        <Switch>
            <Route exact path="/admin">
                <Dashboard/>
            </Route>

            <Route name='Article' path="/admin/articles">
                <Articles/>
            </Route>

            <Route path="/admin/categories">
                <Categories/>
            </Route>
        </Switch>
    )
}

export default AdminRoutes;
