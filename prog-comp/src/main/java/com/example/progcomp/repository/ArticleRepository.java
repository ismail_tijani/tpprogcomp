package com.example.progcomp.repository;

import com.example.progcomp.model.Article;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ArticleRepository extends CrudRepository<Article, Long> {

    Optional<Article> findByName(String name);
}
