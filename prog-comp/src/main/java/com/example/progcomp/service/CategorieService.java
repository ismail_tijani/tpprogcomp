package com.example.progcomp.service;

import com.example.progcomp.model.Categorie;
import com.example.progcomp.model.CategorieDTO;
import com.example.progcomp.model.ArticleDTO;

import java.util.List;
import java.util.Optional;

public interface CategorieService {
    List<Categorie> getAllCategories();

    List<Categorie> getAllCategorieBySaleType(Integer saleType);

    Optional<Categorie> getCategorieById(Long id);

    Optional<Categorie> getCategorieByName(String name);

    Categorie saveNewCategorie(CategorieDTO categorieDTO);

    Categorie updateCategorie(Categorie oldCategorie, CategorieDTO newCategorieDTO);

    void deleteCategorie(Categorie categorie);

    Categorie addNewArticleToCategorie(Long categorieId, ArticleDTO articleDTO);

}
