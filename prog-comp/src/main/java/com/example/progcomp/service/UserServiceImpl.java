package com.example.progcomp.service;

import com.example.progcomp.model.User;
import com.example.progcomp.model.UserDTO;
import com.example.progcomp.model.Article;
import com.example.progcomp.model.ArticleDTO;
import com.example.progcomp.repository.UserRepository;
import com.example.progcomp.repository.ArticleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ArticleRepository articleRepository;

    @Override
    @Transactional
    public List<User> getAllUsers() {
        List<User> userList = new ArrayList<>();
        userRepository.findAll().forEach(userList::add);
        return userList;
    }

    @Override
    @Transactional
    public Optional<User> getUserById(Long id){return userRepository.findById(id);}

    @Override
    @Transactional
    public Optional<User> getUserByEmail(String email){return userRepository.findByEmail(email);}

    @Override
    @Transactional
    public User saveNewUser(UserDTO userDTO) {
        return userRepository.save(convertDTOToUser(userDTO));
    }


    @Override
    @Transactional
    public User updateUser(User oldUser, UserDTO newUserDTO) {
        return userRepository.save(updateUserFromDTO(oldUser, newUserDTO));
    }

    @Override
    @Transactional
    public void deleteUser(User user) {
        userRepository.delete(user);
    }

    private User convertDTOToUser(UserDTO userDTO) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        User user = new User();
        user.setEmail(userDTO.getEmail());
        user.setUserType(userDTO.getUserType());
        user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        return user;
    }

    private User updateUserFromDTO(User user, UserDTO userDTO){
        if(Optional.ofNullable(userDTO.getEmail()).isPresent()){
            user.setEmail(userDTO.getEmail());
        }

        if (Optional.ofNullable((userDTO.getPassword())).isPresent()) {
            user.setPassword(userDTO.getPassword());
        }

        return user;
    }

    @Override
    @Transactional
    public User addNewArticleToUser(Long userId, Long articleId) {
        User user = userRepository.findById(userId).get();
        Article artice = articleRepository.findById(articleId).get();
        user.addArticle(artice);
        return userRepository.save(user);
    }

    private Article convertDTOToArticle(ArticleDTO articleDTO) {
        Article article = new Article();
        article.setName(articleDTO.getName());
        article.setDescription(articleDTO.getDescription());
        article.setColor(articleDTO.getColor());
        article.setType(articleDTO.getType());
        article.setPrice(articleDTO.getPrice());
        article.setCurrentPrice(articleDTO.getCurrentPrice());
        return article;
    }

}