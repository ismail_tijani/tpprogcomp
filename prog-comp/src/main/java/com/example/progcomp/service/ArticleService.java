package com.example.progcomp.service;

import com.example.progcomp.model.Article;
import com.example.progcomp.model.ArticleDTO;

import java.util.List;
import java.util.Optional;

public interface ArticleService {
    List<Article> getAllArticles();

    Optional<Article> getArticleById(Long id);

    Optional<Article> getArticleByName(String name);

    Article saveNewArticle(ArticleDTO articleDTO);

    Article updateArticle(Article oldArticle, ArticleDTO newArticleDTO);

    void deleteArticle(Article article);
}
