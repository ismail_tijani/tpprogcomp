package com.example.progcomp.service;

import com.example.progcomp.model.Article;
import com.example.progcomp.model.ArticleDTO;
import com.example.progcomp.repository.ArticleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class ArticleServiceImpl implements ArticleService {

    private final ArticleRepository articleRepository;

    @Override
    @Transactional
    public List<Article> getAllArticles() {
        List<Article> articleList = new ArrayList<>();
        articleRepository.findAll().forEach(articleList::add);
        return articleList;
    }

    @Override
    @Transactional
    public Optional<Article> getArticleById(Long id){return articleRepository.findById(id);}

    @Override
    @Transactional
    public Optional<Article> getArticleByName(String name){return articleRepository.findByName(name);}

    @Override
    @Transactional
    public Article saveNewArticle(ArticleDTO articleDTO) {
        return articleRepository.save(convertDTOToArticle(articleDTO));
    }


    @Override
    @Transactional
    public Article updateArticle(Article oldArticle, ArticleDTO newArticleDTO) {
        return articleRepository.save(updateArticleFromDTO(oldArticle, newArticleDTO));
    }

    @Override
    @Transactional
    public void deleteArticle(Article article) {
        articleRepository.delete(article);
    }

    private Article convertDTOToArticle(ArticleDTO articleDTO) {
        Article article = new Article();
        article.setName(articleDTO.getName());
        article.setDescription(articleDTO.getDescription());
        article.setColor(articleDTO.getColor());
        article.setType(articleDTO.getType());
        article.setPrice(articleDTO.getPrice());
        article.setCurrentPrice(articleDTO.getCurrentPrice());
        return article;
    }

    private Article updateArticleFromDTO(Article article, ArticleDTO articleDTO){
        if(Optional.ofNullable(articleDTO.getName()).isPresent()){
            article.setName(articleDTO.getName());
        }

        if (Optional.ofNullable((articleDTO.getDescription())).isPresent()) {
            article.setDescription(articleDTO.getDescription());
        }

        if (Optional.ofNullable((articleDTO.getColor())).isPresent()) {
            article.setColor(articleDTO.getColor());
        }

        if (Optional.ofNullable((articleDTO.getType())).isPresent()) {
            article.setType(articleDTO.getType());
        }

        if (Optional.ofNullable((articleDTO.getType())).isPresent()) {
            article.setType(articleDTO.getType());
        }

        if (Optional.ofNullable((articleDTO.getCurrentPrice())).isPresent()) {
            article.setCurrentPrice(articleDTO.getCurrentPrice());
        }
        return article;
    }

}