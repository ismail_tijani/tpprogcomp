package com.example.progcomp.service;

import com.example.progcomp.model.User;
import com.example.progcomp.model.UserDTO;
import com.example.progcomp.model.ArticleDTO;


import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getAllUsers();

    Optional<User> getUserById(Long id);

    Optional<User> getUserByEmail(String email);

    User saveNewUser(UserDTO userDTO);

    User updateUser(User oldUser, UserDTO newUserDTO);

    void deleteUser(User user);

    User addNewArticleToUser(Long userId, Long articleId);

}
