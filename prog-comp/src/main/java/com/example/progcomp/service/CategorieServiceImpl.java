package com.example.progcomp.service;

import com.example.progcomp.model.Categorie;
import com.example.progcomp.model.CategorieDTO;
import com.example.progcomp.model.Article;
import com.example.progcomp.model.ArticleDTO;
import com.example.progcomp.repository.CategorieRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class CategorieServiceImpl implements CategorieService {

    private final CategorieRepository categorieRepository;

    @Override
    @Transactional
    public List<Categorie> getAllCategories() {
        List<Categorie> categorieList = new ArrayList<>();
        categorieRepository.findAll().forEach(categorieList::add);
        return categorieList;
    }

    @Override
    @Transactional
    public Optional<Categorie> getCategorieById(Long id){return categorieRepository.findById(id);}

    @Override
    @Transactional
    public Optional<Categorie> getCategorieByName(String name){return categorieRepository.findByName(name);}

    @Override
    @Transactional
    public List<Categorie> getAllCategorieBySaleType(Integer saleType){return categorieRepository.findAllBySaleType(saleType);}

    @Override
    @Transactional
    public Categorie saveNewCategorie(CategorieDTO categorieDTO) {
        return categorieRepository.save(convertDTOToCategorie(categorieDTO));
    }


    @Override
    @Transactional
    public Categorie updateCategorie(Categorie oldCategorie, CategorieDTO newCategorieDTO) {
        oldCategorie.setName(newCategorieDTO.getName());
        oldCategorie.setSaleType(newCategorieDTO.getSaleType());
        return categorieRepository.save(oldCategorie);
    }

    @Override
    @Transactional
    public void deleteCategorie(Categorie categorie) {
        categorieRepository.delete(categorie);
    }

    @Override
    @Transactional
    public Categorie addNewArticleToCategorie(Long categorieId, ArticleDTO articleDTO) {
        Categorie categorie = categorieRepository.findById(categorieId).get();
        categorie.addArticle(convertDTOToArticle(articleDTO));
        return categorieRepository.save(categorie);
    }

    private Categorie convertDTOToCategorie(CategorieDTO categorieDTO) {
        Categorie categorie = new Categorie();
        categorie.setName(categorieDTO.getName());
        categorie.setSaleType(categorieDTO.getSaleType());
        return categorie;
    }

    private Article convertDTOToArticle(ArticleDTO articleDTO) {
        Article article = new Article();
        article.setName(articleDTO.getName());
        article.setDescription(articleDTO.getDescription());
        article.setColor(articleDTO.getColor());
        article.setType(articleDTO.getType());
        article.setPrice(articleDTO.getPrice());
        article.setCurrentPrice(articleDTO.getCurrentPrice());
        return article;
    }

}