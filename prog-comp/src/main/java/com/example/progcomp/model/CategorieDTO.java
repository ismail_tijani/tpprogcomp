package com.example.progcomp.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategorieDTO {

    @ApiModelProperty(position = 1)
    private String name;

    @ApiModelProperty(position = 2)
    private int saleType;
}
