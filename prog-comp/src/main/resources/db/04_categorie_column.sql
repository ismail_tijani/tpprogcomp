ALTER TABLE article
ADD COLUMN categorie_id INTEGER  REFERENCES categorie(id);

ALTER TABLE article
ADD COLUMN buyer_id INTEGER  REFERENCES auth_user(id);